%!TEX root = ../report.tex
\section{Code size analysis} % (fold)
\label{sec:code_size_analysis}
One of the most basic and discussed\cite{rosenberg1997some} metrics to study complexity of a software project is the size by lines of code (LOC). 
More specifically, the number of lines of code \emph{per file}.
Files with many lines of code indicate that it is probably an important and complex file so if bugs occur in it they are probably hard to fix.
Moreover, high number of lines of code often correlate with low maintainability and high number of defects.

To help us perceive the complexity of the project even better, the size of the code files is viewed as a function of the time.
This shows the evolution of the entire project.
From here on out, please note that these metrics are only calculated for the \texttt{*.c} and \texttt{*.h} files in the \texttt{/app} folder as this folder contains the largest part of the codebase of GIMP.
% \begin{itemize}
% 	\item First, use the “Lines of text counter” calculator to compute the lines-of-code (LOC) metric for the source code files in the repository. This should generate the ‘Code size’ metric. Be forewarned, this plugin might take quite some time to execute, as it needs to actually access the contents of the source files. After the ‘Code size’ metric is available, answer the following questions.
% 	\item How is the size of code files evolving in the project? Are source code files growing or shrinking on the average? Which are the fastest growing files? Which are the files that shrink the most?
% 	\item Group all files, in the evolution view, based on the ‘Code size’ attribute (right-click in the evolution view, and then ‘Group selected’). How much is source code, in terms of percents, from the total project size (in terms of files)?
% \end{itemize}
\subsection{File size analysis}
\label{subsec:file_size_analysis}
"\textit{How is the size of code files evolving in the project? Are source code files growing or shrinking on the average? Which are the fastest growing files? Which are the files that shrink the most?}"


In \autoref{fig:step4:codeSizes} the sizes of the files are shown through time. 
The ``upper half'' of the image shows the \texttt{*.c} files whereas the ``bottom half'' shows the \texttt{*.h} files.
\begin{figure}[t]
	\includegraphics{step4/algemeenKleinerColormap.png}
	\quad
	\includegraphics[width=\linewidth]{step4/algemeenKleiner.png}
	\caption{Color-coded sizes of files in time} \label{fig:step4:codeSizes}
\end{figure}
It is clearly visible that the \texttt{*.h} remain roughly constant in size -- and rather small, whereas the \texttt{*.c} files show more variety.
Therefore we will only focus on the \texttt{*.c} files.
It clearly shows that in the beginning GIMP was one large monolith of a rather small section of very large files -- i.e. larger than hundreds of lines of code.
However, as time progressed we can see that the number of files greatly increased and the number of lines of code dwindled per file.
So overall, the project inflated but the files itself generally got simpler and shorter.

It is interesting too see which files are the fastest growing or shrinking.
This could be an important metric to see which files are in need of a refactoring the most or whether a refactoring was successful, respectively.
When scrolling through the \texttt{*.c} files, we see that the following files are the fastest growing and fastest shrinking files in the repository are visible in \Autoref{fig:step4:shrinker, fig:step4:grower}.

\begin{figure}[t]
	\centering
	\includegraphics{step4/snelstecolormap.png}%[width=\linewidth]
	\caption{Color map used for \Autoref{fig:step4:shrinker, fig:step4:grower}} \label{fig:step4:fastestcolormap}
\end{figure}
\begin{figure}[t]
	\includegraphics[width=\linewidth]{step4/snelsteDaler.png}
	\caption{Fastest shrinking file in the repository} \label{fig:step4:shrinker}
\end{figure}
\begin{figure}[t]
	\includegraphics[width=\linewidth]{step4/snelsteStijger.png}
	\caption{Fastest growing file in the repository.} \label{fig:step4:grower}
\end{figure}

In \autoref{fig:step4:shrinker}, we can clearly see that \texttt{/app/tools/gimpconvolvetool.c} is the fastest shrinker in the repository. 
In four years it has risen from about 300 LOC to somewhere between 700 and 800 LOC, after which it shrunk to less than 300 LOC. 
A few years later it even shrunk to less than 200 LOC.
In the same figure we can see that \texttt{/app/tools/gimpclonetool.c} also shrunk to less than 200 LOC but this took longer than \texttt{gimpconvolvetool.c} and it shrunk more.

In the \autoref{fig:step4:grower}, we can see that \texttt{/app/plug-in/gimppluginprocedure.c} has been steadily rising in just about a year from less than 100 LOC to about 300 LOC, after which in just another year of heavy development it has risen to more than 1000 LOC nowadays.
This makes it a clear winner compared to all other files in the repository. 


\subsection{Portion of code} % (fold)
\label{sub:portion_of_code}
"\textit{How much is source code, in terms of percents, from the total project size (in terms of files)}"

\begin{figure}[ht!]
	\includegraphics{step4/algemeenColormap.png} %[width=\linewidth]
	\quad
	\includegraphics[width=\linewidth]{step4/algemeen.png}
	\caption{Code sorted by analyzed files and is mapped using the colormap next to it}
	\label{fig:step4:algemeen}
\end{figure}

We only analyzed the \texttt{/app}-folder of the GIMP project, as the software we're required to use was not able to gather all the information to continue this report for the whole project in a timely manner. 

As one can see \autoref{fig:step4:algemeen}, the amount of code the GIMP project consists of is about a third of the entire project -- this number we will call \(f_t\). This was estimated by guessing. 
Keep in mind that only the \texttt{app}-folder was analyzed. 
The \texttt{/app}-folder contains 1942 \texttt{C} code- and header files, as described in \autoref{sec:basicrepositoryinvestigation} -- which we will call \(n_{\text{app}}\). 
The total amount of \texttt{C} and header files in the project was calculated to be 2915 -- which we will call \(n_t\). 
This means that to calculate the total amount of source code the following calculation is needed: \[f_c = \frac{f_t}{\left(\frac{n_{\text{app}}}{n_t}\right)} = \frac{\frac{1}{3}}{\left(\frac{1942}{2915}\right)} = 0.5003\].

This means that the total fraction of source code files for the whole project is about half.

The fraction of source code files in the project can also be easily calculated in another way.
Knowing that there are 2915 source code files on a total filebase of 5625 files (according to SolidTA), the fraction of code on the total project is \(\frac{2915}{5625} = 0.5182\). 
We can therefore say that the estimation is quite close to the exact number as the exact fraction of source code of the \texttt{/app}-folder is \(\frac{0.5182}{\left(\frac{2915}{5625}\right)} = 0.34524\).
% subsection portion_of_code (end)5625 files (according to SolidTA).
% section code_size_analysis (end)