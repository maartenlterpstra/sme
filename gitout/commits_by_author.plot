set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'commits_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Commits"
set xtics rotate
set bmargin 6
plot 'commits_by_author.dat' using 1:2 title "Michael Natterer" w lines, 'commits_by_author.dat' using 1:3 title "Sven Neumann" w lines, 'commits_by_author.dat' using 1:4 title "Martin Nordholts" w lines, 'commits_by_author.dat' using 1:5 title "Manish Singh" w lines, 'commits_by_author.dat' using 1:6 title "Mukund Sivaraman" w lines, 'commits_by_author.dat' using 1:7 title "Simon Budig" w lines, 'commits_by_author.dat' using 1:8 title "Marc Lehmann" w lines, 'commits_by_author.dat' using 1:9 title "Marco Ciampa" w lines, 'commits_by_author.dat' using 1:10 title "Tor Lillqvist" w lines, 'commits_by_author.dat' using 1:11 title "William Skaggs" w lines, 'commits_by_author.dat' using 1:12 title "Alexia Death" w lines, 'commits_by_author.dat' using 1:13 title "David Odin" w lines, 'commits_by_author.dat' using 1:14 title "Kevin Cozens" w lines, 'commits_by_author.dat' using 1:15 title "Michael Henning" w lines, 'commits_by_author.dat' using 1:16 title "Jakub Friedl" w lines, 'commits_by_author.dat' using 1:17 title "Daniel Egger" w lines, 'commits_by_author.dat' using 1:18 title "Jehan" w lines, 'commits_by_author.dat' using 1:19 title "Øyvind Kolås" w lines, 'commits_by_author.dat' using 1:20 title "Mikael Magnusson" w lines, 'commits_by_author.dat' using 1:21 title "Alexandre Prokoudine" w lines
