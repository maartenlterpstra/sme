set terminal png transparent size 640,240
set size 1.0,1.0

set terminal png transparent size 640,480
set output 'lines_of_code_by_author.png'
set key left top
set yrange [0:]
set xdata time
set timefmt "%s"
set format x "%Y-%m-%d"
set grid y
set ylabel "Lines"
set xtics rotate
set bmargin 6
plot 'lines_of_code_by_author.dat' using 1:2 title "Michael Natterer" w lines, 'lines_of_code_by_author.dat' using 1:3 title "Sven Neumann" w lines, 'lines_of_code_by_author.dat' using 1:4 title "Martin Nordholts" w lines, 'lines_of_code_by_author.dat' using 1:5 title "Manish Singh" w lines, 'lines_of_code_by_author.dat' using 1:6 title "Mukund Sivaraman" w lines, 'lines_of_code_by_author.dat' using 1:7 title "Simon Budig" w lines, 'lines_of_code_by_author.dat' using 1:8 title "Marc Lehmann" w lines, 'lines_of_code_by_author.dat' using 1:9 title "Marco Ciampa" w lines, 'lines_of_code_by_author.dat' using 1:10 title "Tor Lillqvist" w lines, 'lines_of_code_by_author.dat' using 1:11 title "William Skaggs" w lines, 'lines_of_code_by_author.dat' using 1:12 title "Alexia Death" w lines, 'lines_of_code_by_author.dat' using 1:13 title "David Odin" w lines, 'lines_of_code_by_author.dat' using 1:14 title "Kevin Cozens" w lines, 'lines_of_code_by_author.dat' using 1:15 title "Michael Henning" w lines, 'lines_of_code_by_author.dat' using 1:16 title "Jakub Friedl" w lines, 'lines_of_code_by_author.dat' using 1:17 title "Daniel Egger" w lines, 'lines_of_code_by_author.dat' using 1:18 title "Jehan" w lines, 'lines_of_code_by_author.dat' using 1:19 title "Øyvind Kolås" w lines, 'lines_of_code_by_author.dat' using 1:20 title "Mikael Magnusson" w lines, 'lines_of_code_by_author.dat' using 1:21 title "Alexandre Prokoudine" w lines
