%!TEX root = ../report.tex
\section{Dependency analysis – explore a scenario} % (fold)
\label{sec:dependency_analysis_explore_a_scenario}

When designing tools to analyze source code, there is a need for a data model that is able to store all the data. This should be done in such a way that it is easy to access or calculate the required information. 

To create this data model, it is required that (some parts of the) history of the source code is available. Luckily, version control system such as git and subversion are standard in industry production of software. This allows us to easily fetch the required data. 

In the scenario, it is assumed that the work is done for a starting company, therefor basic functionality is required being the visualization of the call graphs \(G_1, G_2, G_3, ..., G_n\).

However, since companies should grow, the data model should allow for other graphs. These include class inheritance graphs, containment graphs and build dependency graphs. These graphs are all used to analyze software, therefor it should be relatively easy to extend functionality to support these graphs.

\subsection{Generic solution} % (fold)
\label{sub:genericsolution}
The initial thought to show the evolution of a call graph was to solely use transparency to show how long the activity of an edge, where an edge is a call from one object to an other. If an edge has no transparency the edge is still there. The more transparent it gets the longer the edge has been removed.

However, this does not allow to display how recently the edge was created or severed. To do this, color is introduced. An edge is solid green if it was created in the most recent version of the software and fades to black in a certain time. The same holds for a removed edge, but then using a red color.
% subsection idea (generic_solution)

\begin{figure}[h!]
	\centering
	\includegraphics{step6/dependencyview.png} %[width=\linewidth]
	\caption{Visualization of a call graph evolution solution} \label{fig:step6:graphevosol}
\end{figure}

In \autoref{fig:step6:graphevosol} we have created an example of our described generic solution.
This figure represents a call graph at which in this commit 3 no longer calls 4 but now calls 5 instead. 
File 2 was created in a previous commit and calls from 1 to 3 and 4 were also deleted in previous commits.
This figure thus indicates that in a few commits parts from files 3 and 4 refactored to file 2 and parts of file 4 have moved to file 5 as 3 no longer calls 4 but calls 5 instead.

\subsection{Data model} % (fold)
\label{sub:data_model}
In order to visualize or superimpose a graph on another dataset, the graph needs to be stored, naturally.
But before we can think about storing graphs, we need to define what the graphs will \emph{represent}.
A graph is normally defined as \(G = (V, E)\) with \(V\) the set of vertices and \(E = \{(e_1,\,e_2) \mid e_1,\,e_2 \in \wp(V)\}\).
We assume that each commit introduces a new graph so we create versions based on the commit ID.
This means that every edge is defined as an unique pair of vertices. % Hoe zit het dan met 2 nodes die zowel een build als call dependency hebben?
Note that it is not necessarily the case that \(e_1 \neq e_2\).
Also, it implies that one vertex can be connected to multiple other vertices.

One of the best meanings of the vertices in our context are that they represent singular entities.
These are entities that are self-contained and can interact with other entities of the same kind.
In a software project, the most logical units that can assume this role are files.
This makes sense as in a typical software project, code in a certain file interacts with code in another file.
This works especially well in a professional context with object-oriented code where a project is partitioned in classes that interact with each other.
In such projects, every file contains one class, which validates our choice for creating a bijection between files and vertices.

For edges, this correspondence is not as clear.
Depending on the type of graph, an edge can mean something different.
For example, an edge \((e_1,\,e_2)\) can either mean that file \(e_1\) contains the class that is the parent of the class in \(e_2\), or that if the class \(e_1\) is modified then \(e_2\) should also be rebuilt.
These are just examples, an edge between two files can have a myriad of other meanings.

To circumvent this, we need not only to encode whether an edge exists but also what an edge \emph{means}.
In this way each edge is labeled to convey between which files are related and in what way.
Although every edge is unique -- so the edge is only stored once -- it can have multiple meanings attached to it so that the meaning of the edge depends on the context.
\begin{figure}
\centering
\includegraphics[width=\linewidth,clip=true, trim=130 470 10 150]{step6/UMLHaat.pdf}
\caption{An UML description of our model for storing graphs per commit}
\label{fig:step6:model}
\end{figure}
\subsubsection{Efficient implementation} % (fold)
\label{subsub:tefficient_implementation}
Merely one graph of all files in a (semi-)large software projects and relations between the files can grow to a daunting size, let alone multiple graphs over time.
Therefore, it is important to have an efficient implementation of all graphs of a project.
We have visualized our implementation in .\autoref{fig:step6:model}.

We assume that our vertices set \(V\) is fixed. 
That is, \(V\) contains a union of all files that ever existed in the project.

However the set of edges \(E\) vary in time. 
Therefore we propose the following a storage of all graphs in the following way:
Each vertex holds a position in a hash table.
The data contained in each bucket is an object that contains the vertex it is connected to and an EdgeInformation entity.
This EdgeInformation object contains the following: it has a meaning attached to it. In this meaning the type of graph is encoded (e.g. call graph, inheritance graph, etc.) and the directional information. In a real life implementation this can be encoded using an integer where the most significant bit encodes directionality and the rest of the integer is a code for the type of graph.
It also contains a color. This color is green when it is recently added and red when it is recently removed. 
Over time, the color changes from green to white and from red to black.
Also, over time the transparency increases so that irrelevant edges are no longer visible.

This allows for efficient construction of a graph with at a moment as the information can be queried in \(\mathcal{O}(1)\) on average and, since we can assume the graph is rather sparse -- i.e. each graph has \(\mathcal{O}(n)\) edges with \(n\) the number of vertices -- the information stored for each commit id is rather limited.
Although the sparsity assumption does is not theoretically valid, we suspect that for the average software project this assumption \emph{does} hold.

% subsubsection tefficient_implementation (end)

\subsubsection{Trade-offs} % (fold)
\label{subsub:trade_offs}
Our model for storing graphs mainly optimizes for reducing storage space as this can be a limiting factor for usability -- in terms of storage space and speed.
Storing merely one graph for a reasonably sized project is difficult enough as it is, let alone multiple graphs for the same project.
Therefore we chose to optimize for storage space by storing edges only once and attaching meaning to them by binding them to an object providing the meaning of the edge -- i.e. which graph does it occur in.

However, this approach does not come with a few downsides.
For one, git commits IDs are SHA1 sums cannot have a total ordering. 
One cannot say that commit ID \texttt{67f6e6240b0e87af5f6dcbfb694d8d9cd41f494b} is greater than commit ID \texttt{bc4c60e70c69292c3997a47010754fd7320f9fe2} in such a way that it is useful in this context.
This prevents us from another optimization for only storing in which commit an edge is added and in which commit it is -- possibly -- deleted because one cannot easily query whether some commit ID is between two other commit IDs.
We tried to circumvent this by assigning each edge an unique number that maps to commit IDs.
This seems to solve this, but it can still create problems when different branches are considered.
This is because defining a total order on the commits is inherently impossible since it is a partially ordered set.
Therefore, we have no other option but storing a graph for every commit with all the edges in it that have a meaning attached to it.
% subsubsection trade_offs (end)


\subsubsection{New commits} % (fold)
\label{subsub:new_commits}
When a new commit happens one of the first things that obviously needs to happen is to recalculate the relevant graphs.
One could recalculate the \emph{entire} graph, but it is probably more efficient to recalculate from the changes compared to the previous graph.
Note that in this case, it still stores the \emph{entire} graph and not just the changes.
In each commit, every edge in the graph from the previous commit is multiplied with a decay factor which makes sure that an edge is \(0\%\) opaque in \(n\) commits after creation or deletion.

After all edge colors are rescaled, one can determine which edges are added or deleted compared to the previous version of the graph.
These edges will be green (red resp.) with 100\% opaqueness.
% subsubsection new_commits (end)

% subsection data_model (end)

\subsection{Visualization} % (fold)
\label{sub:visualization}

\subsubsection{Solid Trend Analyzer main view} % (fold)
\label{ssub:solid_trend_analyzer_main_view}
\begin{itemize}
	\item Describe a possible way to visualize the evolution of one of the considered dependency graphs (call graph, inheritance graph, containment graph, or build graph) on the same 2D layout as the Solid Trend Analyzer main view. Recall this layout uses the x axis for time and the y axis for files, whereby every file version is drawn as a rectangle. The question is: how to visualize the evolution of a dependency graph atop of this layout? Discuss your solutions to several issues such as: scalability (the proposed visualization should work for real-size repositories such as the one you studied during the previous assignment points); and limited cluttering (the proposed visualization should produce drawings which are reasonably easy to follow, even in the case of a complex, large dataset).
\end{itemize}
It is impossible to implement a visualization for the solution we provided to the Solid Trend Analyzer main view. The main problem is that the x axis cannot be used for time, as it is used to be able to display a graph. One could try to display a graph on a line, but this would create an unworkable mess as a lot of arrows would be crossing each other and introduce clutter. 

An other way to show links would be colors, but this would require some files to have multiple colors, which is not easily displayed. Furthermore, it would also introduce the need to sort on color, but is not possible to display. The problem with sorting on links by color is that links are broken and created thus requiring for files to be shuffled around, which requires the filename to be displayed although there's no room for this.

However, it is possible to implement our visualization in Solid Trend Analyzer. A mock-up of this can bee seen in \autoref{fig:step6:mockup}

\begin{figure}[h!]
	\centering
	\includegraphics[width=1.5\linewidth, rotate=90, center]{step6/mockup.png} %[width=\linewidth]
	\caption{Mockup of graph evolution solution iin SolidTA} \label{fig:step6:mockup}
\end{figure}
% subsubsection solid_trend_analyzer_main_view (end)

\subsubsection{Different graphs} % (fold)
\label{ssub:different_graphs}
% \begin{itemize}
% 	\item The different graph types discussed above (call, inheritance, containment, and build) exhibit some clear differences. These may lead to different decisions in the design of an evolution visualization. Discuss the following two aspects:
% 	\item Which graph is of which of the following types: tree; directed acyclic graph; general (cyclic) graph.
% 	\item A tree is very different from a general cyclic graph. How can you use the knowledge of a particular graph structure in the design of the dependency evolution visualization? Describe in which way you could take advantage in the visualization design if you knew your dependency graph is a tree rather than a general graph.
% \end{itemize}

Of course, not all types of graphs are the same. 
Some types graphs exhibit certain properties which can greatly simplify the graph in question.
We can make the distinction between trees, directed acyclic graphs (DAGs), and general (cyclic) graphs -- to which we shall refer to hereafter as graphs.
In our model, we make no explicit distinction between trees, DAGs, or graphs but their properties can be used in our advantage to create a better visualization.
However, if this distinction \emph{is} made, the quality of the visualization can be improved by exploiting certain properties of the types of graphs.

We have identified the following graphs to be of the respective types
\subparagraph{Trees}
~\\
We have identified that the containment graph is definitely a tree and the inheritance graph is sometimes a tree.
In the case of class containment it is clear that this graph must be a tree. 
Obviously, if class B is contained by class A, it is impossible that A is contained by B simultaneously.
Also, one class can only be contained by one other class simultaneously -- not counting transitive containment.
This clearly yields a tree

The case of the inheritance graph is somewhat more difficult as it depends on the programming language.
If a language only supports singular class inheritance, each class can have only one parent.
In this case too, it is impossible that B is a child of A, while A is a child of B.
So this case is also a tree.
The other case we will discuss in another section.

\subparagraph{DAG}
~\\
Directed acyclic graphs are a type of graph \(G = (V, E)\) such that it holds that \(\{(v_1, v_2), (v_2, v_3), \ldots, (v_n, v_1)\} \nsubseteq E\) -- i.e. there is no directed path in the edge set such that it is possible to start at a vertex and return at that vertex following that path.
We have identified two types of graphs that are actually DAGs.

First, we have identified the build graph as a DAG. 
It is very possible that one file needs be recompiled when one of multiple other files are modified, which rules out that it is a graph.
However, it is unlikely that there is a cyclomatic dependency on build dependency as this would result in a uncompilable project.
Therefore we figure that this graph is a DAG.

Moreover, it is also possible that the inheritance graph is a DAG. 
This can happen when multiple inheritance is allowed by the programming language.
This is for example possible in languages as Python, C++, Scala, Perl and Common Lisp.
This can manifest itself in the ``Deathly Diamond of Death'' \cite{martin1997java}.

\subparagraph{Graph}
All that remains is the graph that can possibly contain cycles.
We have identified as the call graph as this type of graph.
It is possible that class A calls class B, which in turn calls class C which calls class A.
This is a perfectly valid type of program flow and therefore it is a general -- possibly cyclic -- graph.

As mentioned before, we currently make no distinction between the type of graph that is being drawn when a graph is visualized.
However, because of the properties of trees and DAGs, the visualization might be improved as opposed to drawing it as a general graph.
For example, if it is known that a certain type of graph is a tree then it can be visualized without edge crossings which is beneficial to the quality of visualization.
This can be done by imposing a different ordering on the files which allows the edges to be drawn without crossings.
This might also be done for DAGs, however it could be that the edge crossings cannot be entirely eliminated.
However, they can still be minimized compared to a naive graph drawing.

The number of edge crossings can also be minimized if a technique like force-directed graph placement is employed \cite{terpstrakruiger2015}. 
% subsubsection different_graphs (end)


% subsection visualization (end)


% section dependency_analysis_explore_a_scenario (end)